# Logs parser

This program is :
 - Parsing a ip to country database (included in folder ip2nation)
 - Parsing the logs (default is access.log, change it to the access-short to faster runtime)
 - use some function to extract data (top senders / countries, extract in a time range, etc...)

## Instructions

```
python3 main.py
```