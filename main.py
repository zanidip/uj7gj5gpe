#!/usr/bin/env python3
import ipaddress
import re
from datetime import datetime
from collections import Counter
import sys
from timeit import default_timer as timer

log_file = './access.log'
ip2nation_file = './ip2nation/ip2nation.csv'
countries_file = './ip2nation/countries.csv'

countries = {}
ip2nation = {}

logdb = []

# First to program will parse files in ip2nation folder and build dictionnaries from the data.
# That will allow us to get country from IP while parsing the logs

def parse_ip2nation():
    global countries
    global ip2nation

    with open(countries_file,"r") as f:
        for line in f: 
            code, country = line.split(',',2)
            countries[code] = country

    ip2nation_unsorted = {}
    with open(ip2nation_file,"r") as f:
        for line in f: 
            ip, code = line.replace('\n','').split(',',2)
            ip2nation_unsorted[int(ip)] = code
    # We have to parse the dict to get the correct range while covering it from the IP
    ip2nation = {i:ip2nation_unsorted[i] for i in sorted(ip2nation_unsorted, reverse=True )}

def get_country(ip):
    if(len(ip.split('.')) != 4):
        return 'ERR'
    search_key = int(ipaddress.IPv4Address(ip))
    res = next(ip2nation[key] for key in ip2nation if key < search_key) 

    return res


def parse_logs(database=logdb):

    # memorise the regex that will parse the logs :
    pat = re.compile('(?P<ip>.*) - [a-zA-Z-]+ \[(?P<date>.*) \+0800\] .*')

    with open(log_file, 'r') as f :

        for line in f :

            result = pat.match(line)
            # automaticaly write ip and date in the dict
            log = result.groupdict()

            # format date in datetime format 
            log['date'] = datetime.strptime(log['date'], '%d/%b/%Y:%H:%M:%S')
            # Add country code from IP
            log['country'] = get_country(log['ip'])
            
            database.append(log)

            # sometimes show progress to user
            if( len(database) % 5000 == 0): print(' ...' + str(len(database))+ ' done')

# Build a new logs DB from the initial one between 2 dates
def extract_time(date_start, date_end, database=logdb):
    # intialise the list we'll return
    output_db = []

    for log in database:
        date = log['date']

        # If date match, append this log
        if( date_start < date and date < date_end ):
            output_db.append(log)

    return output_db

# Get <size> number of top item grouped by <key>
# For ex. get_top(ip,5) will get the top 5 IPs   
def get_top(key,size,database=logdb):

    values=[log[key] for log in database]
    return Counter(values).most_common(size)

    # this function can be used to check that IP2NATION list is correct by testing it on some ips
def check_ip2nation():
    print('should be hk : ' + str(int(ipaddress.IPv4Address("14.0.156.41"))) + get_country('14.0.156.41'))
    print('should be hk : ' + str(int(ipaddress.IPv4Address("220.241.213.49"))) + get_country('220.241.213.49'))
    print('should be fr : ' + str(int(ipaddress.IPv4Address("2.2.2.4"))) + get_country('2.2.2.4'))

def main():

    # Parsing Data
    print('parsing ip2nation...')
    parse_ip2nation()

    print('parsing logs...')
    parse_logs()

    # Get number of requests
    print('\ntotal number of HTTP requests : %d \n' % len(logdb))

    # get top sender from 10 to 19 june
    
    date_start=datetime(2019,6,10)
    date_end=datetime(2019,6,19,23,59,59)

    june_logs = extract_time(date_start,date_end)

    top10_june_logs = get_top('ip',10,june_logs)

    print('top 10 sender from  %s  to  %s  :' % (date_start, date_end))
    for ip, requests in top10_june_logs:
        print("IP : %s \t Requests : %d" % (ip,requests))

    # get country sending the more logs :

    print('\ncountry with most requests originating from :')
    top_country = get_top('country',1)
    # get only first value, then the code of the country
    code_country = top_country[0][0]
    print(countries[code_country.replace(' ','')])


if(__name__ == "__main__"):
    main()
